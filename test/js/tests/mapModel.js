define(['jquery','qunit','model/mapModel'], function($,qunit,MapModel){

    test( "MapModel Defaults", function() {
        var model = new MapModel();
        var result = model.get('center');
        equal( typeof result, 'object', "map.center is an object" );
    });
});