/*global QUnit, require*/
'use strict';
require.config({
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        },
        epoxy: {
            deps: [
                'backbone'
            ]
        },
       qunit: {
           exports: 'QUnit',
           init: function() {
               QUnit.config.autoload = false;
               QUnit.config.autostart = false;
               window.Qunit = this.Qunit;
               return this.QUnit;
           },
        },
        'sinon-qunit': {
            deps: [
                'qunit',
                'sinon'
            ]
        }
    },
    baseUrl: '/resources/js',
    paths: {
        ac: 'lib/ac',
        jquery: '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min',
        backbone: 'lib/backbone',
        underscore: 'lib/underscore',
        text: 'lib/text',
        async: 'lib/async',
        epoxy: 'lib/backbone.epoxy',
        qunit: '/test/lib/qunit/qunit',
        sinon: '/test/lib/sinon/sinon',
        'sinon-qunit': '/test/lib/sinon/sinon-qunit',
        'tests/mapModel': '/test/js/tests/mapModel',
        'tests/markerModel': '/test/js/tests/markerModel',
        'tests/editorView': '/test/js/tests/editorView',
        'tests/formView': '/test/js/tests/formView',
        'tests/markerFormView': '/test/js/tests/markerFormView',
        'tests/mapPreviewView': '/test/js/tests/mapPreviewView',
        'tests/markerListView': '/test/js/tests/markerListView',
    }
});
require([
    "backbone",
    "underscore",
    "qunit",
    "sinon",
    "sinon-qunit",
    "tests/mapModel",
    "tests/markerModel",
    "tests/editorView",
    'tests/formView',
    'tests/markerFormView',
    'tests/mapPreviewView',
    'tests/markerListView',
], function(Backbone, _, Qunit) {
    Backbone.Notifications = {};
    _.extend(Backbone.Notifications, Backbone.Events);

    QUnit.load();
    QUnit.start();
});
