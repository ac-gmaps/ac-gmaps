/*global require, _,AP*/
'use strict';
require.config({
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        },
        epoxy: {
            deps: [
                'backbone'
            ]
        }
    },
    paths: {
        ac: 'lib/ac',
        jquery: '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min',
        backbone: 'lib/backbone',
        underscore: 'lib/underscore',
        text: 'lib/text',
        async: 'lib/async',
        epoxy: 'lib/backbone.epoxy'
    }
});

require([
    'jquery',
    'backbone',
    'ac',
    'views/editor/editorView',
    'model/mapModel'
], function($, Backbone, ACjs, EditorView, MapModel){
    //create a pubsub event system for global notifications
    Backbone.Notifications = {};
    _.extend(Backbone.Notifications, Backbone.Events);

    var map,
    data = AC.getUrlParam('data', true);

    if(data){
        data = JSON.parse(data);
    } else {
        data = {};
    }

    var view = new EditorView({
        el: $('#container'),
        model: new MapModel(data)
    });

    view.render();
    AC.removeMargin();
});