/*global define, AP, pubsub */
'use strict';
define([
  'jquery',
  'underscore',
  'backbone',
  'text!template/editor/editorTemplate.html',
  'views/editor/formView',
  'views/editor/mapPreviewView',
  'collection/markerCollection',
  'model/mapModel',
  'epoxy'
], function($, _, Backbone, editorTemplate, FormView, MapPreviewView, MarkerCollection, MapModel){

    var editorView = Backbone.Epoxy.View.extend({
        events: {
            'click #savebutton': 'save',
            'click #debug': 'debug'
        },
        initialize: function(options){
            if(_.isObject(this.model.attributes.markers) === false){
                this.model.set('markers', new MarkerCollection());
            }
            AP.require(["confluence", "dialog", "events"], this.bindSubmit);
        },
        render: function(){
            //template
            this.$el.html(editorTemplate);
            //render form view
            this.formView = new FormView({
                el: $('#form', this.$el),
                model: this.model
            });
            //render the map view
            this.mapPreviewView = new MapPreviewView({
                el: $('#map', this.$el),
                model: this.model,
            });

            return this;
        },
        save: function(confluence){
            var data = JSON.stringify(this.model);
            AP.require(["confluence"], function(confluence){
                confluence.saveMacro({
                    'data': data
                });
            });
        },
        bindSubmit: function(confluence, dialog){
            //this is currently not working.
            dialog.getButton("submit").bind(this.save);
        }
    });

    return editorView;
});