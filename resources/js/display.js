/*global require, _,AP,AC*/
'use strict';
require.config({
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        },
    },
    paths: {
        ac: 'lib/ac',
        jquery: '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min',
        backbone: 'lib/backbone',
        underscore: 'lib/underscore',
        text: 'lib/text',
        async: 'lib/async',
        epoxy: 'lib/backbone.epoxy'
    }
});

require([
    'jquery',
    'backbone',
    'ac',
    'views/display/mapView',
    'model/mapModel',
], function($, Backbone, ACjs, MapView, MapModel){

    var view = new MapView({
        el: $('#container'),
        model: new MapModel(JSON.parse(AC.getUrlParam('data', true)))
    });
    view.render();
});